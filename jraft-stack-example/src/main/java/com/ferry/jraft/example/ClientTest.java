package com.ferry.jraft.example;

import lombok.extern.slf4j.Slf4j;

/**
 * @Author ferry
 * @create 2022/5/27 19:50
 * @description
 */
@Slf4j
public class ClientTest {
    public static void main(String[] args) throws InterruptedException {

        StackClient client = new StackClient();
        int id1 = client.create();
        int id2 = client.create();
        int id3 = client.create();
        Thread.sleep(6000);
        client.push(id1, 5);
        client.push(id2, 6);
        client.add(id1);
        log.info("-------------------------------------------");
        log.info("push 5,push 6,add,get:{}", client.get(id3));
        client.inc(id2);
        client.get(id1);
        log.info("inc,get:{}", client.get(id1));
        log.info("-------------------------------------------");
    }
}
