package com.ferry.jraft;

import com.ferry.jraft.model.LogEntry;

/**
 * @Author ferry
 * @create 2022/5/19 17:08
 * @description 日志管理模块
 */
public interface LogModule extends LifeCycle{
    /**
     * 写日志
     *
     * @param logEntry
     */
    void write(LogEntry logEntry);

    /**
     * 读日志
     *
     * @param index
     * @return
     */
    LogEntry read(long index);

    /**
     * 从指定位置开始移除日志
     *
     * @param startIndex
     */
    void removeOnStartIndex(long startIndex);

    /**
     * 获取最后一条日志
     *
     * @return 最后一条日志
     */
    LogEntry getLast();

    /**
     * 获取最后一条日志的索引
     *
     * @return 最后一条日志的索引
     */
    long getLastIndex();
}
