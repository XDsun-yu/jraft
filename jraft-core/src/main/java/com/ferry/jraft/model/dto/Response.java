package com.ferry.jraft.model.dto;

import java.io.Serializable;

/**
 * @Author ferry
 * @create 2022/5/22 11:35
 * @description 响应的通用父类
 */
public class Response implements Serializable {
    public String str;
}
