package com.ferry.jraft.model.state;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author ferry
 * @create 2022/5/17 16:14
 * @description 所有服务器上的易失性变量
 */
@Data
public class VolatileState implements Serializable {

    private static final long serialVersionUID = 1284685896228116813L;

    /**
     * 已提交的日志条目的最大索引
     */
    private volatile long commitIndex;

    /**
     * 已应用到状态机上的日志条目的最大索引
     */
    private volatile long lastApplied;
}
