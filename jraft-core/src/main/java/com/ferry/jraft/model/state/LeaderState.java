package com.ferry.jraft.model.state;

import com.ferry.jraft.model.Peer;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

/**
 * @Author ferry
 * @create 2022/5/17 16:22
 * @description
 */
@Data
public class LeaderState implements Serializable {

    private static final long serialVersionUID = -7434186016275748986L;

    /**
     * leader发送到其他服务器的下一个日志条目的索引(初始化为leader最后的日志条目的索引+1)
     */
    private Map<Peer,Long> nextIndex;

    /**
     * leader已经复制到其他服务器的日志条目的最大索引(初始化为0，单调递增)
     */
    private Map<Peer,Long> matchIndex;

}
