package com.ferry.jraft.model.dto;

import com.ferry.jraft.model.LogEntry;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @Author ferry
 * @create 2022/5/17 15:26
 * @description 追加条目请求参数
 */
@Data
@Builder
public class AppendEntriesRequest extends Request implements Serializable {

    private static final long serialVersionUID = 7809556592910065034L;

    /**
     * leader的任期
     */
    private long term;

    /**
     * leader的Id
     */
    private int leaderId;

    /**
     * 新日志条目之前的日志条目的索引
     */
    private long prevLogIndex;

    /**
     * 新日志条目之前的日志条目的任期
     */
    private long prevLogTerm;

    /**
     * 需要被保存的日志条目
     */
    private List<LogEntry> entries;

    /**
     * leader已提交的日志条目的最大索引
     */
    private long leaderCommit;
}
