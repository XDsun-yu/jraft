package com.ferry.jraft.model.dto;

import lombok.Data;

/**
 * @Author ferry
 * @create 2022/5/20 15:13
 * @description client发来的请求参数
 */
@Data
public class ClientRequest extends Request {

    private Object param;

    public ClientRequest(Object param) {
        this.param = param;
        this.cmd = CLIENT_REQUEST;
    }
}
