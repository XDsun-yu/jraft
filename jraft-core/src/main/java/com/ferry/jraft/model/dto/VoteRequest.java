package com.ferry.jraft.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author ferry
 * @create 2022/5/17 15:58
 * @description 请求投票请求参数
 */
@Builder
@Data
public class VoteRequest extends Request implements Serializable {

    private static final long serialVersionUID = 5372846022988343780L;

    /**
     * candidate的任期
     */
    private long term;

    /**
     * candidate的Id
     */
    private int candidateId;

    /**
     * candidate最后一个条目的索引
     */
    private long lastLogIndex;

    /**
     * candidate最后一个条目的任期
     */
    private long lastLogTerm;

}
