package com.ferry.jraft.model.state;

import com.ferry.jraft.LogModule;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author ferry
 * @create 2022/5/17 16:06
 * @description 所有服务器的持久化状态
 */
@Data
public class PersistentState implements Serializable {

    private static final long serialVersionUID = 4849254662627721698L;

    /**
     * server所认知到的最高任期
     */
    private volatile long currentTerm;

    /**
     * 当前任期内收到投票的candidateId
     */
    private volatile int votedFor;

    /**
     * 日志条目
     */
    public LogModule logModule;

}
