package com.ferry.jraft.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author ferry
 * @create 2022/5/22 11:30
 * @description 请求参数的通用父类
 */
@Data
public class Request implements Serializable {
    private static final long serialVersionUID = -2430466902992769263L;

    /** 请求投票 */
    public static final int REQUEST_VOTE = 0;
    /** 附加日志 */
    public static final int APPENDE_ENTRIES = 1;
    /** 客户端 */
    public static final int CLIENT_REQUEST = 2;
    /** 添加成员 */
    public static final int CHANGE_PEERS_ADD = 3;
    /** 删除成员 */
    public static final int CHANGE_PEERS_REMOVE = 4;
    /** 请求类型 */
    public int cmd = -1;
}
