package com.ferry.jraft.model.dto;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

/**
 * @Author ferry
 * @create 2022/5/17 15:57
 * @description 追加条目请求的响应
 */
@Builder
@Data
public class AppendEntriesResponse extends Response implements Serializable {

    private static final long serialVersionUID = -1581230382106538345L;

    private long term;

    private boolean success;

    public AppendEntriesResponse(long term, boolean success) {
        this.term = term;
        this.success = success;
    }

    public AppendEntriesResponse(boolean success) {
        this.success = success;
    }
}
