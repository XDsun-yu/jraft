package com.ferry.jraft.model.dto;

import lombok.Builder;
import lombok.Data;

/**
 * @Author ferry
 * @create 2022/5/20 15:14
 * @description 响应给client的结果
 */
@Data
@Builder
public class ClientResponse extends Response {

    /**
     * 是否成功
     */
    boolean success;

    /**
     * 返回结果
     */
    Object res;
}
