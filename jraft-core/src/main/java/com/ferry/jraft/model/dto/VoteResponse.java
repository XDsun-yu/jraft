package com.ferry.jraft.model.dto;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author ferry
 * @create 2022/5/17 16:02
 * @description 请求投票请求的响应
 */
@Data
public class VoteResponse extends Response implements Serializable {

    private static final long serialVersionUID = 3305397931330430401L;

    private long term;

    private boolean voteGranted;

    public VoteResponse(boolean voteGranted) {
        this.voteGranted = voteGranted;
    }

    public VoteResponse(long term, boolean voteGranted) {
        this.term = term;
        this.voteGranted = voteGranted;
    }
}
