package com.ferry.jraft.model;

/**
 * @Author ferry
 * @create 2022/5/21 14:26
 * @description 单个节点的配置类
 */
public class Peer {

    private final int id;

    private final int port;

    private final String ip;

    private final String addr;

    public Peer(int id, String addr) {
        this.id = id;
        this.addr = addr;
        int index = addr.indexOf(':');
        ip = addr.substring(0, index);
        port = Integer.parseInt(addr.substring(index + 1, addr.length()));
    }

    public Peer(int id, String ip, int port) {
        this.id = id;
        this.ip = ip;
        this.port = port;
        this.addr = ip + port;
    }

    public int getPort(){
        return this.port;
    }

    public int getId() {
        return this.id;
    }

    public String getAddr() {
        return addr;
    }

    @Override
    public int hashCode() {
        return addr.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Peer peer = (Peer) obj;
        return this.addr == peer.addr;
    }

    @Override
    public String toString() {
        return "Peer{" +
                "id=" + id +
                ", addr='" + addr + '\'' +
                '}';
    }
}
