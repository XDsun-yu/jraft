package com.ferry.jraft;

import com.ferry.jraft.model.dto.AppendEntriesRequest;
import com.ferry.jraft.model.dto.AppendEntriesResponse;
import com.ferry.jraft.model.dto.VoteRequest;
import com.ferry.jraft.model.dto.VoteResponse;

/**
 * @Author ferry
 * @create 2022/5/19 16:47
 * @description 一致性管理模块
 */
public interface ConsensusModule {
    /**
     * 向其他节点请求投票
     *
     * @param voteRequest
     * @return 投票结果
     */
    VoteResponse requestVote(VoteRequest voteRequest);

    /**
     * 向其他节点追加条目
     *
     * @param appendEntriesRequest
     * @return 追加结果
     */
    AppendEntriesResponse appendEntries(AppendEntriesRequest appendEntriesRequest);
}
