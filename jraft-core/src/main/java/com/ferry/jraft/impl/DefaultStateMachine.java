package com.ferry.jraft.impl;

import com.ferry.jraft.StateMachine;
import com.ferry.jraft.model.LogEntry;
import com.sun.org.apache.xpath.internal.operations.String;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author ferry
 * @create 2022/5/23 17:01
 * @description 状态机的默认实现类
 */
@Slf4j
public class DefaultStateMachine implements StateMachine {
    @Override
    public void init() throws Throwable {
        log.info("StateMachine init");
    }

    @Override
    public void destroy() throws Throwable {
        log.info("StateMachine destroy");
    }

    @Override
    public Object apply(LogEntry logEntry) {
        log.warn("logEnrty:{} has been applied to stateMachine",logEntry);
        return "1";
    }
}
