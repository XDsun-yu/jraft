package com.ferry.jraft.concurrent;

import java.util.concurrent.ThreadFactory;

/**
 * @Author ferry
 * @create 2022/5/21 15:41
 * @description
 */
public class RaftThreadFactory implements ThreadFactory {
    @Override
    public Thread newThread(Runnable r) {
        Thread t=new Thread(r,"Raft Thread");
        t.setUncaughtExceptionHandler(new RaftUncaughtExceptionHandler());
        t.setDaemon(true);
        t.setPriority(5);
        return t;
    }
}
