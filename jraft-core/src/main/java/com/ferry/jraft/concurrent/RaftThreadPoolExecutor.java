package com.ferry.jraft.concurrent;

import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @Author ferry
 * @create 2022/5/21 15:05
 * @description 线程池
 */
@Slf4j
public class RaftThreadPoolExecutor extends ThreadPoolExecutor {

    private static int coreNum = Runtime.getRuntime().availableProcessors();
    private static int maxPoolSize = coreNum * 2;
    private static final int queueSize = 1024;
    private static final long keepTime = 1000 * 60;
    private static TimeUnit keepTimeUnit = TimeUnit.MILLISECONDS;

    public RaftThreadPoolExecutor() {
        super(coreNum + 1, maxPoolSize, keepTime, keepTimeUnit, new LinkedBlockingQueue<>(queueSize), new RaftThreadFactory());
    }

    @Override
    protected void beforeExecute(Thread t, Runnable r) {
    }

    @Override
    protected void afterExecute(Runnable r, Throwable t) {
    }

    @Override
    protected void terminated() {
    }
}
