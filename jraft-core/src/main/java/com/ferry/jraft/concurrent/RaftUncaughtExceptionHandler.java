package com.ferry.jraft.concurrent;

import lombok.extern.slf4j.Slf4j;

/**
 * @Author ferry
 * @create 2022/5/21 15:37
 * @description 异常处理
 */
@Slf4j
public class RaftUncaughtExceptionHandler implements Thread.UncaughtExceptionHandler {
    @Override
    public void uncaughtException(Thread t, Throwable e) {
        log.warn("Exception occurred from thread {}", t.getName(), e);
    }
}
