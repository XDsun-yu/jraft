package com.ferry.jraft;

/**
 * @Author ferry
 * @create 2022/5/19 17:11
 * @description 管理各个模块的生命周期
 */
public interface LifeCycle {

    public void init() throws Throwable;

    public void destroy() throws Throwable;
}
