package com.ferry.jraft;

import com.ferry.jraft.model.Peer;
import com.ferry.jraft.model.PeerGroup;
import com.ferry.jraft.model.dto.*;

/**
 * @Author ferry
 * @create 2022/5/20 14:28
 * @description 节点
 */
public interface Node extends LifeCycle{

    /**
     * 加载配置
     *
     * @param peerConfig
     * @param peerGroup
     */
    public void loadConfig(Peer peerConfig, PeerGroup peerGroup);

    /**
     * 加载状态机
     *
     * @param stateMachine
     */
    public void loadStateMachine(StateMachine stateMachine);

    /**
     * 处理追加条目请求
     *
     * @param appendEntriesRequest
     * @return 响应结果
     */
    public AppendEntriesResponse handleAppendEntriesRequest(AppendEntriesRequest appendEntriesRequest);

    /**
     * 处理投票请求
     *
     * @param voteRequest
     * @return 投票结果
     */
    public VoteResponse handleVoteRequest(VoteRequest voteRequest);

    /**
     * 处理client的请求
     *
     * @param clientRequest
     * @return 响应结果
     */
    public ClientResponse handleClientRequest(ClientRequest clientRequest);

}
