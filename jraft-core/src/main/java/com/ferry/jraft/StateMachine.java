package com.ferry.jraft;

import com.ferry.jraft.model.LogEntry;

/**
 * @Author ferry
 * @create 2022/5/19 17:10
 * @description 状态机模块
 */
public interface StateMachine extends LifeCycle{
    /**
     * 把日志应用到状态机
     *
     * @param logEntry
     * @return
     */
    public Object apply(LogEntry logEntry);

}
