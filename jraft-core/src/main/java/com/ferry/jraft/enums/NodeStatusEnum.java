package com.ferry.jraft.enums;

/**
 * @Author ferry
 * @create 2022/5/17 21:09
 * @description 节点状态
 */
public enum NodeStatusEnum {

    LEADER("LEADER", "主节点"),
    CANDIDATE("CANDIDATE", "候选节点"),
    FOLLOWER("FOLLOWER", "从节点"),;

    private String code;

    private String desc;

    NodeStatusEnum(String code, String desc) {
        this.code=code;
        this.desc=desc;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

}
