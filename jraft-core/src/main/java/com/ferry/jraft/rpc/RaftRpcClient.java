package com.ferry.jraft.rpc;


import com.alipay.remoting.exception.RemotingException;
import com.alipay.remoting.rpc.RpcClient;
import com.ferry.jraft.LifeCycle;
import com.ferry.jraft.model.dto.Request;
import com.ferry.jraft.model.dto.Response;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.TimeUnit;

/**
 * @Author ferry
 * @create 2022/5/20 14:04
 * @description 节点的RPC client实现类
 */
@Slf4j
public class RaftRpcClient implements LifeCycle {
    private final RpcClient CLIENT = new RpcClient();

    public Response send(Request request,String url,int timeout) throws RemotingException {
        Response response;
        try {
            response= (Response) CLIENT.invokeSync(url,request,timeout);
            return response;
        }  catch (InterruptedException e) {

        }
        return null;
    }

    public Response send(Request request,String url) throws RemotingException {
        return send(request,url,500);
    }


    @Override
    public void init() throws Throwable {
        CLIENT.startup();
    }

    @Override
    public void destroy() throws Throwable {
        CLIENT.shutdown();
    }
}
