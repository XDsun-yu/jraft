package com.ferry.jraft.rpc;

import com.alipay.remoting.AsyncContext;
import com.alipay.remoting.BizContext;
import com.alipay.remoting.DefaultBizContext;
import com.alipay.remoting.RemotingContext;
import com.alipay.remoting.rpc.protocol.MultiInterestUserProcessor;
import com.alipay.remoting.rpc.protocol.SyncUserProcessor;
import com.ferry.jraft.model.dto.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executor;

/**
 * @Author ferry
 * @create 2022/5/20 16:13
 * @description bolt需要的请求处理器
 */
public class RaftUserProcessor implements MultiInterestUserProcessor<Request> {

    @Override
    public List<String> multiInterest() {
        List<String> list=new ArrayList<>();
        list.add(Request.class.getName());
        list.add(VoteRequest.class.getName());
        list.add(AppendEntriesRequest.class.getName());
        list.add(ClientRequest.class.getName());
        return list;
    }


    @Override
    public BizContext preHandleRequest(RemotingContext remotingCtx, Request request) {
        return new DefaultBizContext(remotingCtx);
    }

    @Override
    public void handleRequest(BizContext bizCtx, AsyncContext asyncCtx, Request request) {
        throw new UnsupportedOperationException(
                "ASYNC handle request is unsupported in SyncUserProcessor!");
    }

    @Override
    public Object handleRequest(BizContext bizCtx, Request request) throws Exception {
        //此处先不进行真正的逻辑处理，在RPC sever处再根据请求类型调用不同的功能处理
        return null;
    }

    @Override
    public String interest() {
        return null;
    }

    @Override
    public Executor getExecutor() {
        return null;
    }

    @Override
    public boolean processInIOThread() {
        return false;
    }

    @Override
    public boolean timeoutDiscard() {
        return true;
    }

    @Override
    public void setExecutorSelector(ExecutorSelector executorSelector) {

    }

    @Override
    public ExecutorSelector getExecutorSelector() {
        return null;
    }
}
