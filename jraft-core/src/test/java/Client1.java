import com.ferry.jraft.model.dto.ClientRequest;
import com.ferry.jraft.model.dto.ClientResponse;
import com.ferry.jraft.model.dto.Request;
import com.ferry.jraft.rpc.RaftRpcClient;
import lombok.extern.slf4j.Slf4j;

/**
 * @Author ferry
 * @create 2022/5/25 13:35
 * @description
 */
@Slf4j
public class Client1 {
    public static void main(String[] args) throws Throwable {
        RaftRpcClient client = new RaftRpcClient();
        client.init();

        Request request = new ClientRequest(String.valueOf(1));
        request.setCmd(Request.CLIENT_REQUEST);
        ClientResponse response = (ClientResponse) client.send(request, "localhost:8775");

    }
}
