import com.ferry.jraft.Node;
import com.ferry.jraft.impl.DefaultStateMachine;
import com.ferry.jraft.impl.NodeImpl;
import com.ferry.jraft.model.Peer;
import com.ferry.jraft.model.PeerGroup;
import lombok.extern.slf4j.Slf4j;


/**
 * @Author ferry
 * @create 2022/5/24 15:02
 * @description 节点的启动类
 */
@Slf4j
public class TestBoot {
    static int i = 1;

    public static void main(String[] args) throws Throwable {
        PeerGroup group=new PeerGroup(3);
        Peer peer1=new Peer(1,"localhost:8775");
        Peer peer2=new Peer(2,"localhost:8776");
        Peer peer3=new Peer(3,"localhost:8777");

        group.addPeer(peer1);
        group.addPeer(peer2);
        group.addPeer(peer3);

        Node node1=new NodeImpl();
        node1.loadConfig(peer1,group);
        node1.loadStateMachine(new DefaultStateMachine());
        node1.init();

        Thread.sleep(2000);

        Node node2=new NodeImpl();
        node2.loadConfig(peer2,group);
        node2.loadStateMachine(new DefaultStateMachine());
        node2.init();

        Thread.sleep(2000);

        Node node3=new NodeImpl();
        node3.loadConfig(peer3,group);
        node3.loadStateMachine(new DefaultStateMachine());
        node3.init();

    }

}
